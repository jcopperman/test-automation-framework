﻿using TestFramework.Configuration;
using TestFramework.Helpers;

namespace TestFramework.Base
{
    public abstract class BaseStep : Base
    {
        public virtual void NavigateSite()
        {
            DriverContext.Browser.GoToUrl(Settings.Environment);
            LogHelpers.Write("Opened the browser");
        }
    }
}
