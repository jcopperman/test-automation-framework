I'm a framework that simplifies test automation with Selenium by applying the Page Object Model pattern using features of Selenium.Support.PageObjects. I enable simple, clear and readable tests.

A test project is included in this repository that demonstrates the use of the framework. The test project contains automated UI tests, bound to SpecFlow feature files, which automates Ui testing against an ASP.Net web application, hosted on Azure at: http://qupwebdrwho.azurewebsites.net/ 

Example Test:

        [Binding]
        [Scope(Tag = "smoke")]
        [Scope(Tag = "positive")]
        public class LoginTests : BaseStep
            {
                [Given(@"I have navigated to the application")]
                public void GivenIHaveNavigatedToTheApplication()
                    {
                        NavigateSite();
                        CurrentPage = GetInstance<HomePage>();
                    }
        
                [Given(@"I see the homepage")]
                public void GivenISeeTheHomepage()
                    {
                        CurrentPage.As<HomePage>().CheckIfLoginExist();
                    }

                [Then(@"I click login")]
                public void ThenIClickLogin()
                    {
                        CurrentPage.As<HomePage>().ClickLogin();
                    }

                [When(@"I enter valid information")]
                public void WhenIEnterUsernameAndPassword(Table table)
                    {
                        dynamic data = table.CreateDynamicInstance();
            
                        CurrentPage.As<LoginPage>().Login(data.UserName, data.Password);
                    }

                [Then(@"I click Submit button")]
                    public void ThenIClickLoginButton()
                    {
                        CurrentPage = CurrentPage.As<LoginPage>().ClickLoginButton();
                    }

                [Then(@"I should see the username")]
                public void ThenIShouldSeeTheUsername()
                    {
                        if (CurrentPage.As<HomePage>().GetLoggedInUser().Contains("admin"))
                            System.Console.WriteLine("Login successful");
                        else
                            System.Console.WriteLine("Unsuccessful login");
                    }
                }