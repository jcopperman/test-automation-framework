﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using TestFramework.Base;

namespace TestProject.Pages
{
    internal class CharacterListPage : BasePage
    {
        [FindsBy(How = How.Name, Using = "searchTerm")]
        IWebElement txtSearch { get; set; }

        [FindsBy(How = How.LinkText, Using = "Create New")]
        IWebElement lnkCreateNew { get; set; }

        [FindsBy(How = How.ClassName, Using = "table")]
        IWebElement tblEmployeeList { get; set; }


        public CreateCharacterPage ClickCreateNew()
        {
            lnkCreateNew.Click();
            return new CreateCharacterPage();
        }

        public IWebElement GetEmployeeList()
        {
            return tblEmployeeList;
        }
    }
}
