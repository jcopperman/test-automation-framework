﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using TestFramework.Base;

namespace TestProject.Pages
{
    internal class CreateCharacterPage : BasePage
    {
        [FindsBy(How = How.Id, Using = "CharacterName")]
        IWebElement txtName { get; set; }

        [FindsBy(How = How.Id, Using = "Description")]
        IWebElement txtDescription { get; set; }

        [FindsBy(How = How.Id, Using = "Actor")]
        IWebElement txtActor { get; set; }

        [FindsBy(How = How.Id, Using = "ActorDateOfBirth")]
        IWebElement dateActorDOB { get; set; }

        [FindsBy(How = How.Id, Using = "IsFavourite")]
        IWebElement chkIsFavourite { get; set; }

        [FindsBy(How = How.Id, Using = "btnSubmit")]
        IWebElement btnCreate { get; set; }

        internal void ClickCreateButton()
        {
            btnCreate.Submit();
        }

        internal void CreateEmployee(string name, string description, string dateofbirth)
        {
            txtName.SendKeys(name);
            txtDescription.SendKeys(description);
            txtActor.SendKeys(dateofbirth);
            chkIsFavourite.Click();
        }
    }
}
