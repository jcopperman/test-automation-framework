﻿Feature: CreateCharacter
	As a Doctor Who fan
	I want to create Doctor Who characters
	So that I can store a collection of characters

@smoke @positive
Scenario: Create Doctor Who character
	Given I have navigated to the application
	And I see the homepage
	Then I click Characters
	Then I click Create New
	When I enter valid information
	| CharacterName | Description | Actor | DateOfBirth |
	| Dalek   | The Daleks are a fictional extraterrestrial race of mutants in the British science fiction television series Doctor Who, created by the megalomaniacal scientist Davros of the planet Skaro to be an emotionless "master race" bent on universal conquest and domination, utterly without pity, compassion or remorse. | Nicholas Briggs | 1961/09/29 |
	Then I click Submit
	Then The information is saved successfully
